var Class = function(){};

Class.extend = function(prop) {
  var _super = this.prototype;

  function F() {}
  F.prototype = _super;
  var proto = new F();

  for (var name in prop) {
    //
    // esta parte de las funciones se puede
    // saltar en un primer paso
    //
    if (typeof prop[name] == "function" &&
        typeof _super[name] == "function") {
      proto[name] = (function(name, fn) {
        return function() {
          var tmp = this._super;
          this._super = _super[name];
          var ret = fn.apply(this, arguments);
          this._super = tmp;
          return ret;
        }
      })(name, prop[name]);
    } else {
      proto[name] = prop[name];
    }
  }

  function Klass() {
    if (this.init) this.init.apply(this, arguments);
  }

  Klass.prototype = proto;
  Klass.prototype.constructor = Klass;

  Klass.extend = this.extend;
  return Klass;
};

var Person = Class.extend({
  init: function(name) {
    console.log("Bienvenido, " + name);
    this.name = name;
  },
  bailar: function() {
    console.log("tanana nana, nana, nana...");
  },
  piernas: 2
});

var Ninja = Person.extend({
  init: function() {
    console.log("NINJA EN ACCION!");
    this._super("ninja");
  },
  bailar: function() {
    console.log("Los ninjas no bailan!");
  },
  esgrimeEspada: true
});

var n = new Ninja(); // NINJA EN ACCION!\n Bienvenido, ninja
n.bailar(); // Los ninjas no bailan!
n instanceof Ninja && n instanceof Person; // true
n.piernas; // 2
n.esgrimeEspada; // true
